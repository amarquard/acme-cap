
![acmecap-icon](logo/academic-cap-header2.png)

# acme-cap: a Clinical Microbiomics efficient customised analysis package it from bitbucket using

The goal of acme-cap is to become an R package that can replace the
current `core-analysis` R markdowns, that we use for analysing acme-map
output.

## Installation

You can install it from bitbucket using:

``` r
devtools::install_bitbucket("amarquard/acme-cap", auth_user = "amarquard", password = "FXNpArMbZ5QGbXHjL8jM")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(acmecap)
## basic example code
dat <- cm_read_data("../../../../studies/beigsa/analysis-fecal2/000_prepare_metadata/beigsa.meta.rds")
#> 203cef308ac6328e8b44f1cecbb29d82  2020-02-25 09:49:33  /Volumes/Secomba/andrea/Boxcryptor/Dropbox (CM)/Main/studies/beigsa/analysis-fecal2/000_prepare_metadata  andrea
cm_write_xlsx(dat, "dummy.xlsx")
```

What is special about using `README.Rmd` instead of just `README.md`?
You can include R chunks like so:

``` r
summary(cars)
#>      speed           dist       
#>  Min.   : 4.0   Min.   :  2.00  
#>  1st Qu.:12.0   1st Qu.: 26.00  
#>  Median :15.0   Median : 36.00  
#>  Mean   :15.4   Mean   : 42.98  
#>  3rd Qu.:19.0   3rd Qu.: 56.00  
#>  Max.   :25.0   Max.   :120.00
```

You’ll still need to render `README.Rmd` regularly, to keep `README.md`
up-to-date.

You can also embed plots, for example:

In that case, don’t forget to commit and push the resulting figure
files, so they display on GitHub\!
