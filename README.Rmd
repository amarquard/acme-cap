---
output: github_document
---

```{r, include = FALSE}
### README.md is generated from README.Rmd (this file that you are redong now). Please edit this file, and then render it ('knit').
```

```{r, include = FALSE}

knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "man/figures/README-",
  out.width = "100%"
)
```

![acmecap-icon](logo/academic-cap-header2.png)

# acme-cap: a Clinical Microbiomics efficient customised analysis package it from bitbucket using
The goal of acme-cap is to become an R package that can replace the current `core-analysis` R markdowns, that we use for analysing acme-map output.

## Installation

You can install it from bitbucket using:

``` r
devtools::install_bitbucket("amarquard/acme-cap", auth_user = "amarquard", password = "FXNpArMbZ5QGbXHjL8jM")
```

## Example

This is a basic example which shows you how to solve a common problem:

```{r example}
library(acmecap)
## basic example code
dat <- cm_read_data("../../../../studies/beigsa/analysis-fecal2/000_prepare_metadata/beigsa.meta.rds")
cm_write_xlsx(dat, "dummy.xlsx")
```

What is special about using `README.Rmd` instead of just `README.md`? You can include R chunks like so:

```{r cars}
summary(cars)
```

You'll still need to render `README.Rmd` regularly, to keep `README.md` up-to-date.

You can also embed plots, for example:

```{r pressure, echo = FALSE}
#plot(pressure)
```

In that case, don't forget to commit and push the resulting figure files, so they display on GitHub!
