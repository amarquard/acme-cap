#' Read and write rds files etc
#'
#' @param file Filename of .rds file to read
#'
#' @return
#' @export
#'
#' @examples
cm_read_rds <- function(file) {
  out <- readRDS(file)
  message(paste(attr(out, "cm.signature"), collapse = "  "))
  out
}

